export const isEmailValid = (email: string) => {
  return /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email);
};

export const isPassWordValid = (password: string) => {
  return password?.length > 5;
};

export const doPasswordsMatch = (password1: string, password2: string) => {
  return password1 === password2;
};
