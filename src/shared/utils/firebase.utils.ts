import { constants } from "shared/constants";
import { initializeApp } from "firebase/app";

export const initializeFirebaseInstance = () => {
  const {
    API_KEY,
    APP_ID,
    PROJECT_ID,
    AUTH_DOMAIN,
    STORAGE_BUCKET,
    MEASUREMENT_ID,
    MESSAGING_SENDER_ID,
  } = constants.firebaseConfig;
  return initializeApp({
    apiKey: API_KEY,
    appId: APP_ID,
    authDomain: AUTH_DOMAIN,
    projectId: PROJECT_ID,
    storageBucket: STORAGE_BUCKET,
    messagingSenderId: MESSAGING_SENDER_ID,
    measurementId: MEASUREMENT_ID,
  });
};
