import { UserCredential } from "firebase/auth";
import React, { createContext, PropsWithChildren } from "react";

type AuthContextProps = {
  isLoggedIn?: boolean;
  user?: UserCredential;
  setIsLoggedOut?: () => void;
  setIsLoggedIn?: (user: UserCredential) => void;
  removeUser?: () => void;
};

export const AuthContext = createContext<AuthContextProps>({});

export const AuthContextProvider: React.FC<
  PropsWithChildren<AuthContextProps>
> = ({
  isLoggedIn,
  user,
  children,
  setIsLoggedIn,
  setIsLoggedOut,
  removeUser,
}) => {
  return (
    <AuthContext.Provider
      value={{ isLoggedIn, user, setIsLoggedIn, setIsLoggedOut, removeUser }}
    >
      {children}
    </AuthContext.Provider>
  );
};
