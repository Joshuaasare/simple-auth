import isEmpty from "lodash.isempty";
import { constants } from "shared/constants";
import sjcl from "sjcl";

export function readFromWebStorage(key: string, isEncrypted?: boolean) {
  const data = window.localStorage.getItem(key) ?? "";
  const dataToSend =
    !isEmpty(data) && isEncrypted
      ? sjcl.decrypt(constants.app.JEST_TEST_KEY, JSON.parse(data))
      : data;

  return dataToSend === "null" ? null : dataToSend;
}

export function saveToWebStorage(
  key: string,
  value: string,
  encrypt?: boolean
): void {
  const valueToStore = encrypt
    ? JSON.stringify(sjcl.encrypt(constants.app.JEST_TEST_KEY, value))
    : value;
  window.localStorage.setItem(key, valueToStore as string);
}

export function deleteFromWebStorage(key: string): void {
  window.localStorage.removeItem(key);
}
