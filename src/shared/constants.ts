export const constants = {
  firebaseConfig: {
    API_KEY: process.env.REACT_APP_FIREBASE_API_KEY,
    AUTH_DOMAIN: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    DATABASE_URL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
    PROJECT_ID: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    STORAGE_BUCKET: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    MESSAGING_SENDER_ID: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
    APP_ID: process.env.REACT_APP_FIREBASE_APP_ID,
    MEASUREMENT_ID: process.env.REACT_APP_FIREBASE_MEASURENT_ID,
  },

  app: {
    JEST_TEST_KEY: process.env.JEST_TEST_KEY ?? "jest_test_key",
    USER_CREDENTIAL: "user_credential",
  },
};
