import React, { useContext } from "react";
import { AuthContext } from "shared/contexts/AuthContext";
type Props = {};

const MainComponent: React.FC<Props> = ({}) => {
  const { setIsLoggedOut, user, removeUser } = useContext(AuthContext);
  return (
    <div className="App">
      <h1>Congrats! You're loggedin to SimpleAuth as {user?.user.email}</h1>

      <span className="link">
        <span onClick={() => setIsLoggedOut?.()}>Logout</span>
      </span>

      <span className="link">
        <span onClick={() => removeUser?.()}>Delete Account</span>
      </span>
    </div>
  );
};

export default MainComponent;
