import { getAuth, UserCredential } from "firebase/auth";
import isEmpty from "lodash.isempty";
import React, { useCallback, useEffect, useState } from "react";
import LoginComponent from "shared/components/LoginComponent";
import RegisterComponent from "shared/components/RegisterComponent";
import { constants } from "shared/constants";
import { deleteUser } from "firebase/auth";
import { AuthContextProvider } from "shared/contexts/AuthContext";
import {
  deleteFromWebStorage,
  readFromWebStorage,
} from "shared/services/storageService";
import MainComponent from "./MainComponent";
import Swal from "sweetalert2";

export enum PageState {
  login = "LOGIN",
  register = "REGISTER",
}

const AuthComponent: React.FC = () => {
  const [pageState, setPageState] = useState<PageState>(PageState.login);
  const [loggedIn, setLoggedIn] = useState<boolean | undefined>();
  const [user, setUser] = useState<UserCredential | undefined>();
  const [loading, setLoading] = useState<boolean>(true);

  const initializeAuth = useCallback(async () => {
    const storedUser = readFromWebStorage(constants.app.USER_CREDENTIAL, true);
    const user = !isEmpty(storedUser)
      ? (JSON.parse(storedUser ?? "") as UserCredential)
      : undefined;

    if (user?.user) {
      setUser(user);
      setLoggedIn(true);
      setLoading(false);
    } else {
      setUser(undefined);
      setLoggedIn(false);
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    initializeAuth();
  }, [initializeAuth]);

  const updatePageState = (newPage: PageState) => {
    setPageState(newPage);
  };

  const setIsLoggedIn = (user?: UserCredential) => {
    setUser(user);
    setLoggedIn(true);
  };

  const setIsLoggedOut = () => {
    deleteFromWebStorage(constants.app.USER_CREDENTIAL);
    setUser(undefined);
    setLoggedIn(false);
  };

  const removeUser = async () => {
    const auth = getAuth();
    if (auth.currentUser) {
      try {
        await deleteUser(auth.currentUser);
        Swal.fire({
          title: "Congrats",
          text: "Account removed",
          icon: "success",
          confirmButtonText: "Proceed",
          didClose: () => setIsLoggedOut(),
        });
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: (err as Error).message.split("Firebase: Error")?.[1],
          text: "Something went wrong! Please try again",
        });
      }
    }
  };

  const renderContent = () => {
    if (loggedIn) {
      return <MainComponent />;
    }

    if (pageState === PageState.login) {
      return <LoginComponent updatePageState={updatePageState} />;
    }

    return <RegisterComponent updatePageState={updatePageState} />;
  };

  return (
    <AuthContextProvider
      isLoggedIn={loggedIn}
      user={user}
      setIsLoggedIn={setIsLoggedIn}
      setIsLoggedOut={setIsLoggedOut}
      removeUser={removeUser}
    >
      {!loading && renderContent()}
    </AuthContextProvider>
  );
};

export default AuthComponent;
