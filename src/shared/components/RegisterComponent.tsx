import { Formik } from "formik";
import React from "react";
import { PageState } from "shared/components/AuthComponent";
import {
  doPasswordsMatch,
  isEmailValid,
  isPassWordValid,
} from "shared/utils/form.utils";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import Swal from "sweetalert2";

type Props = {
  updatePageState: (newPage: PageState) => void;
};

type Fields = {
  email: string;
  password: string;
  confirmedPassword: string;
};

const RegisterComponent: React.FC<Props> = ({ updatePageState }) => {
  const validationErrors = (values: Fields) => {
    const { email, password, confirmedPassword } = values;
    return {
      email: !isEmailValid(email) && "Invalid email address",
      password:
        !isPassWordValid(password) &&
        "Password should be at least 6 characters",
      confirmedPassword: getPasswordErrorMessage(password, confirmedPassword),
    };
  };

  const getPasswordErrorMessage = (password1: string, password2: string) => {
    if (!isPassWordValid(password2)) {
      return "Password should be at least 6 characters";
    }

    if (!doPasswordsMatch(password1, password2)) {
      return "Passwords do not match";
    }

    return undefined;
  };

  return (
    <div className="app">
      <h1>Register to Simple Auth</h1>

      <Formik
        initialValues={{
          email: "",
          password: "",
          confirmedPassword: "",
        }}
        validate={(values) => {
          if (Object.values(validationErrors(values)).every((val) => !val)) {
            return undefined;
          }

          return validationErrors(values);
        }}
        onSubmit={async (values) => {
          const { email, password } = values;
          const auth = getAuth();
          try {
            await createUserWithEmailAndPassword(auth, email, password);
            Swal.fire({
              title: "Congrats",
              text: "Account created",
              icon: "success",
              confirmButtonText: "Proceed",
              didClose: () => updatePageState(PageState.login),
            });
          } catch (err) {
            Swal.fire({
              icon: "error",
              title: (err as Error).message.split("Firebase: Error")?.[1],
              text: "Something went wrong!",
            });
          }
        }}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            dirty,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
            handleReset,
          } = props;
          return (
            <form onSubmit={handleSubmit}>
              <label htmlFor="email" style={{ display: "block" }}>
                Email
              </label>
              <input
                id="email"
                placeholder="Enter your email"
                type="text"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                className={
                  errors.email && touched.email
                    ? "text-input error"
                    : "text-input"
                }
              />
              {errors.email && touched.email && (
                <div className="input-feedback">{errors.email}</div>
              )}

              <label htmlFor="password" style={{ display: "block" }}>
                Password
              </label>
              <input
                id="password"
                placeholder="Enter your password"
                type="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                className={
                  errors.password && touched.password
                    ? "text-input error"
                    : "text-input"
                }
              />
              {errors.password && touched.password && (
                <div className="input-feedback">{errors.password}</div>
              )}

              <label htmlFor="password" style={{ display: "block" }}>
                Confirm Password
              </label>
              <input
                id="confirmedPassword"
                placeholder="Re-enter password"
                type="password"
                value={values.confirmedPassword}
                onChange={handleChange}
                onBlur={handleBlur}
                className={
                  errors.confirmedPassword && touched.confirmedPassword
                    ? "text-input error"
                    : "text-input"
                }
              />
              {errors.confirmedPassword &&
                touched.confirmedPassword &&
                touched.password && (
                  <div className="input-feedback">
                    {errors.confirmedPassword}
                  </div>
                )}

              <button
                type="button"
                className="outline"
                onClick={handleReset}
                disabled={!dirty || isSubmitting}
              >
                Reset
              </button>
              <button type="submit" disabled={isSubmitting}>
                Register
              </button>
            </form>
          );
        }}
      </Formik>

      <span className="link">
        <label>Already have an account?</label>
        <span onClick={() => updatePageState(PageState.login)}>Login Here</span>
      </span>
    </div>
  );
};

export default RegisterComponent;
