import React, { useContext } from "react";
import { Formik } from "formik";
import { PageState } from "shared/components/AuthComponent";
import { isEmailValid, isPassWordValid } from "shared/utils/form.utils";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import Swal from "sweetalert2";
import { AuthContext } from "shared/contexts/AuthContext";
import { saveToWebStorage } from "shared/services/storageService";
import { constants } from "shared/constants";

type Props = {
  updatePageState: (newPage: PageState) => void;
};

type Fields = {
  email: string;
  password: string;
};

const LoginComponent: React.FC<Props> = ({ updatePageState }) => {
  const { setIsLoggedIn } = useContext(AuthContext);
  const validationErrors = (values: Fields) => {
    const { email, password } = values;

    return {
      email: !isEmailValid(email) && "Invalid email address",
      password:
        !isPassWordValid(password) &&
        "Password should be at least 6 characters",
    };
  };

  return (
    <div className="app">
      <h1>Login to Simple Auth</h1>

      <Formik
        initialValues={{ email: "", password: "" }}
        validate={(values) => {
          if (Object.values(validationErrors(values)).every((val) => !val)) {
            return undefined;
          }

          return validationErrors(values);
        }}
        onSubmit={async (values) => {
          const { email, password } = values;
          const auth = getAuth();
          try {
            const user = await signInWithEmailAndPassword(
              auth,
              email,
              password
            );

            setIsLoggedIn?.(user);
            saveToWebStorage(
              constants.app.USER_CREDENTIAL,
              JSON.stringify(user),
              true
            );
          } catch (err) {
            Swal.fire({
              icon: "error",
              title: (err as Error).message.split("Firebase: Error")?.[1],
              text: "Something went wrong!",
            });
          }
        }}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            dirty,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
            handleReset,
          } = props;
          return (
            <form onSubmit={handleSubmit}>
              <label htmlFor="email" style={{ display: "block" }}>
                Email
              </label>
              <input
                id="email"
                placeholder="Enter your email"
                type="text"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                className={
                  errors.email && touched.email
                    ? "text-input error"
                    : "text-input"
                }
              />
              {errors.email && touched.email && (
                <div className="input-feedback">{errors.email}</div>
              )}

              <label htmlFor="password" style={{ display: "block" }}>
                Password
              </label>
              <input
                id="password"
                placeholder="Enter your password"
                type="password"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                className={
                  errors.password && touched.password
                    ? "text-input error"
                    : "text-input"
                }
              />
              {errors.password && touched.password && (
                <div className="input-feedback">{errors.password}</div>
              )}

              <button
                type="button"
                className="outline"
                onClick={handleReset}
                disabled={!dirty || isSubmitting}
              >
                Reset
              </button>
              <button type="submit" disabled={isSubmitting}>
                Login
              </button>
            </form>
          );
        }}
      </Formik>

      <span className="link">
        <label>Don't have an account?</label>
        <span onClick={() => updatePageState(PageState.register)}>
          Register Here
        </span>
      </span>
    </div>
  );
};

export default LoginComponent;
