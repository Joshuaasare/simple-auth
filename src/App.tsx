import React, { useEffect } from "react";
import "./App.css";
import AuthComponent from "shared/components/AuthComponent";
import { initializeFirebaseInstance } from "shared/utils/firebase.utils";

const App = () => {
  useEffect(() => {
    initializeFirebaseInstance();
  }, []);

  return <AuthComponent />;
};

export default App;
